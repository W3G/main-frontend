import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TiposService {
  private apiURL = 'http://main-sl.com/API/index.php?token=AKD5S4X169JLXIPQ742&data=tipos';

  constructor(private http: HttpClient) { }

  getTipos(){
    return this.http.get(this.apiURL);
  }
}
