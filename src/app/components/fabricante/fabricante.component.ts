import { Component, OnInit } from '@angular/core';
import { MarcasService } from '../../services/marcas.service';

@Component({
  selector: 'app-fabricante',
  templateUrl: './fabricante.component.html',
  styleUrls: ['./fabricante.component.css']
})
export class FabricanteComponent implements OnInit {
  fabricantes: object;

  constructor(private marcasService: MarcasService) { }

  ngOnInit() {
    this.marcasService.getMarcas().subscribe( data =>{
      this.fabricantes = data;
    });
  }

}
